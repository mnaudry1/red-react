import { Alert } from "react-bootstrap";
import { Link } from "react-router-dom";

const Page404 = () => {
    return (
        <Alert variant="danger" >
            <Alert.Heading>Oh snap! You got an 404 error!</Alert.Heading>
            <p>
                This page is not availlable. <Link to='/'>Cilck here</Link> to go Home page.
            </p>
        </Alert>
    );
}


export default Page404;