import {Container,Image} from 'react-bootstrap';
import RedNavBar from '../components/RedNavBar';
import PostList from '../components/PostList';
import React, { useState, useEffect } from 'react'
import axios from 'axios';
import '../css/custom-image.css';

const HomPage = () => {

    const initialBannerData = {
        src:null,
        loading: true,
        isBanner:false,
      }

    const url = `${process.env.REACT_APP_WORDPRESS_URL}/wp-json/red/v1/custom-header`;

    const [banner, setBanner] = useState(initialBannerData);

    useEffect(() => {
        const ac = new AbortController();
        const getBanner = async () => {
          return  await axios(
            url,{signal: ac.signal}
          )
        }
          getBanner().then((response)=>{
        
            (response.status===200)?
            setBanner({src:response.data.src,loading:false,isBanner:true}):
            setBanner({src:response.data.src,loading:false,isBanner:false});
          }).catch((error)=>{
              setBanner({...banner,loading:false});
          })

          return ()=> ac.abort();
      },[]) // eslint-disable-line react-hooks/exhaustive-deps
    

     return (
         <div>
            <RedNavBar/>
            <Container>
                {
                    (banner.isBanner)?
                        <Image className='mt-3' src={banner.src} fluid />:
                        <div>No banner avaiallble</div>
                }
                <PostList></PostList>
            </Container>
        </div>
     );
}


export default HomPage;