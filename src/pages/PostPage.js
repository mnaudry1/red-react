import RedNavBar from '../components/RedNavBar';
import {Container,Row,Col,Image} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from 'axios';
import '../css/post.css';
import '../css/custom-image.css';

const PostPage = () => {

    const postIntialData = {
        post: {},
        loading: true,
    }
    const params = useParams();

    const url = `${process.env.REACT_APP_WORDPRESS_URL}/wp-json/red/v1/post/${params.postId}`;

    const [post, setPost] = useState(postIntialData);

    useEffect(() => {
        const ac = new AbortController();
        const getPost = async () => {
          return  await axios(
            url,{signal: ac.signal}
          )
        }
          getPost().then((response)=>{
              console.log(response.data);
            (response.status===200)?
            setPost({post :response.data
                ,loading:false}):
            setPost({...post,loading:false});
            
          }).catch((error)=>{
            setPost({...post,loading:false});
          })
          return ()=> ac.abort();
      }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div>
            <RedNavBar/>
            <Container>
                <Row>
                    <Col className='text-center'><h3>{post.post.title}</h3></Col>
                </Row>
                <Row>
                    <Col className='text-center'><h5>{post.post.author}, <span className="red-opacity">{post.post.published_at}</span></h5></Col>
                </Row>
  
                {
                    (post.post.featured_image)?
                    <Image src={post.post.featured_image.src} fluid />
                    :<div/>
                }
                <div className='text-justify mt-5'  dangerouslySetInnerHTML={{__html: post.post.content}}/>
            </Container>
        </div>
    );
}


export default PostPage;