import {  Routes, Route } from "react-router-dom";
import HomPage from "./pages/HomePage";
import Page404 from "./pages/Page404";
import PostPage from "./pages/PostPage";
const  App = () => {
  return (
    <Routes>
      <Route path="/" element={<HomPage />} />
      <Route path="post/:postId" element={<PostPage />} />
      <Route path="*" element={<Page404/>}/>
    </Routes>
  );
}

export default App;
