import {Card,Button,Col} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
import None from '../images/none.jpg';

const PostCard = (props) => {
    const {posts} = props;
    return (
        <Col className='mb-4'>
            <Card>
                {(posts.thumbnail.src)?
                    <Card.Img variant="top" src={posts.thumbnail.src} />:
                    <Card.Img variant="top" src={None} />
                }
                <Card.Body>
                    <Card.Title>{posts.title}</Card.Title>
                    <Card.Text>
                    {posts.excerpt}
                    </Card.Text>
                    <LinkContainer to={`/post/${posts.id}`}>
                        <Button variant="primary">more ...</Button>
                    </LinkContainer>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">{posts.published_at} by {posts.author}</small>
                </Card.Footer>
            </Card>
        </Col>
        )
}

export default PostCard ;