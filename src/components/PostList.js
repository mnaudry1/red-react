import React, { useState, useEffect } from 'react';
import {Row} from 'react-bootstrap';
import PostCard from './PostCard';
import axios from 'axios';
const PostList = () =>{
    const postsIntialData = {
        posts: [],
        loading: true,
    }

    const url = `${process.env.REACT_APP_WORDPRESS_URL}/wp-json/red/v1/last-posts`;

    const [posts, setPosts] = useState(postsIntialData);

    useEffect(() => {
        const ac = new AbortController();
        const getBanner = async () => {
          return  await axios(
            url,{signal: ac.signal}
          )
        }
          getBanner().then((response)=>{
            (response.status===200)?
            setPosts({posts:response.data,loading:false}):
            setPosts({posts:response.data,loading:false});
            
          }).catch((error)=>{
            setPosts({...posts,loading:false});
          })
          return ()=> ac.abort();
      }, []) // eslint-disable-line react-hooks/exhaustive-deps
    

    return (
        <div className='mt-5'>
            <h3 className='text-center mb-5'>Posts</h3>
            {(posts.loading)?
                <div>Data loding ...</div>:
                <Row xs={1} sm={2} >  
                        {posts.posts.map((post)=><PostCard key={post.id} posts={post} />)}
                </Row>
            }      
        </div>);
        
}

export default PostList;