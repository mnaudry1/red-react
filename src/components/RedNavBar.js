import {Container,Navbar,Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
const RedNavBar = ()=>{

    return (
    <Navbar bg="primary" variant="dark" expand="lg">
        <Container>
        <LinkContainer to="/"><Navbar.Brand>Red</Navbar.Brand></LinkContainer>
            <Nav className="me-auto">
                <LinkContainer to="/">
                    <Nav.Link >Home</Nav.Link>
                </LinkContainer>
            </Nav>
        </Container>
    </Navbar>) ;
}

export default RedNavBar;